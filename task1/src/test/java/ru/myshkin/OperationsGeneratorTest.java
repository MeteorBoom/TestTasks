package ru.myshkin;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Year;
import java.util.Arrays;
import java.util.List;

class OperationsGeneratorTest {

    private OperationsGenerator operationsGenerator;
    private String[] officesNumbers;
    private InputStream officesStream;
    private ByteArrayOutputStream operationsStream;
    private Year year;

    @BeforeEach
    void setUp() {
        operationsGenerator = new OperationsGenerator();
        officesNumbers = new String[]{"1", "2", "3"};
        officesStream = new ByteArrayInputStream(String.join("\n", officesNumbers).getBytes());
        operationsStream = new ByteArrayOutputStream();
        year = Year.of(2016);
    }

    @Test
    public void generate_operationCountLessThanZero_throwException() throws IOException {
        //given
        int invalidOperationsCount = -6;

        //then
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> operationsGenerator.generate(officesStream, invalidOperationsCount, operationsStream, year));
    }

    @Test
    public void generate_correctParameters_correctOperationsCountInValidValuesDiapasons() throws IOException {
        //when
        operationsGenerator.generate(officesStream, 5, operationsStream, year);

        //then
        String operationsFileData = new String(operationsStream.toByteArray());
        List<String> officesNumbersList = Arrays.asList(officesNumbers);

        for (String line : operationsFileData.split("\n")) {
            String[] splittedLine = line.split(", ");

            LocalDate date = LocalDate.parse(splittedLine[0]);
            LocalDate minDate = year.atMonth(1).atDay(1);
            LocalDate maxDate = year.plusYears(1).atMonth(1).atDay(1);
            Assertions.assertTrue((date.isAfter(minDate) || date.equals(minDate)) && date.isBefore(maxDate));

            String timeString = splittedLine[1];
            Assertions.assertDoesNotThrow(() -> {
                LocalTime.parse(timeString);
            });

            String officeNumber = splittedLine[2];
            Assertions.assertTrue(officesNumbersList.contains(officeNumber));

            float sum = Float.valueOf(splittedLine[4]);
            Assertions.assertTrue(sum > 10_000 && sum < 100_000);
        }
    }
}