package ru.myshkin;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        if (args.length < 3) {
            throw new IllegalArgumentException("Three parameters must be specified: " +
                    "[file name with list of office numbers] [operations count] [operations file name]");
        }

        String officesFileName = args[0];
        int operationsCount = Integer.parseInt(args[1]);
        String operationsFileName = args[2];

        new OperationsGenerator()
                .generate(officesFileName, operationsCount, operationsFileName);
    }
}
