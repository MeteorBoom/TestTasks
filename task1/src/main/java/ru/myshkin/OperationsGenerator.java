package ru.myshkin;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Year;
import java.util.Locale;
import java.util.Random;

public class OperationsGenerator {
    private Random random = new Random();

    /**
     * Генерирует файл с операциями за предыдущий год
     *
     * @param officesFileName    имя файла со списком идентификаторов офисов. В каждой строке содержится один
     *                           идентификатор
     * @param operationsCount    количество операций, которых нужно сгенерировать
     * @param operationsFileName имя файла, в который будут записываться сгенерированные операции
     * @throws IOException возникает в случае ошибок чтения/записи файлов
     */
    public void generate(String officesFileName, int operationsCount, String operationsFileName) throws IOException {
        try (FileInputStream officesInputStream = new FileInputStream(officesFileName);
             FileOutputStream operationsOutputStream = new FileOutputStream(operationsFileName)) {
            generate(officesInputStream, operationsCount, operationsOutputStream, Year.now().minusYears(1));
        }
    }

    /**
     * Генерирует операции в пределах указанного года и записывает результат в указанный OutputStream
     *
     * @param officesInputStream     входной поток, в каждой строке которого содержится идентификатор офиса
     * @param operationsCount        количество операций, которых нужно сгенерировать
     * @param operationsOutputStream выходной поток, в который будут записываться сгенерированные операции
     * @param year                   год, в пределах которого будут генерироваться операции
     * @throws IOException возникает в случае ошибок чтения/записи потоков
     */
    public void generate(
            InputStream officesInputStream,
            int operationsCount,
            OutputStream operationsOutputStream,
            Year year) throws IOException {

        if (operationsCount < 0) {
            throw new IllegalArgumentException("Count of operations can't be less or equal to 0");
        }

        String[] officesNumbers = readOfficesNumbers(officesInputStream);

        try (OutputStreamWriter writer = new OutputStreamWriter(operationsOutputStream, StandardCharsets.UTF_8);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            for (int operationNumber = 0; operationNumber < operationsCount; operationNumber++) {
                LocalDateTime dateTime = generateDateTime(year);
                String operationLine = String.format(Locale.US, "%s, %s, %s, %d, %.2f",
                        dateTime.toLocalDate(),
                        dateTime.toLocalTime(),
                        officesNumbers[random.nextInt(officesNumbers.length)],
                        operationNumber,
                        generateSum());

                bufferedWriter.write(operationLine);
                bufferedWriter.newLine();
            }
        }
    }

    private static String[] readOfficesNumbers(InputStream officesInputStream) throws IOException {
        try (InputStreamReader reader = new InputStreamReader(officesInputStream, StandardCharsets.UTF_8);
             BufferedReader bufferedReader = new BufferedReader(reader)) {

            return bufferedReader.lines()
                    .toArray(String[]::new);
        }
    }

    private LocalDateTime generateDateTime(Year year) {
        LocalDateTime startDateTime = getStartOfYear(year);
        LocalDateTime endDateTime = getStartOfYear(year.plusYears(1));
        int minutesInPrevYear = (int) Duration.between(startDateTime, endDateTime).toMinutes();

        return startDateTime.plusMinutes(
                random.nextInt(minutesInPrevYear));
    }

    private static LocalDateTime getStartOfYear(Year year) {
        return year.atMonth(1)
                .atDay(1)
                .atTime(0, 0);
    }

    private float generateSum() {
        int minSum = 10_000;
        int maxSum = 100_000;
        int sumDivInKopeck = (maxSum - minSum) * 100;

        return minSum + (float) random.nextInt(sumDivInKopeck) / 100;
    }
}
