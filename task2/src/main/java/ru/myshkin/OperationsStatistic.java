package ru.myshkin;

import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.ChoiceFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

public class OperationsStatistic {

    /**
     * Вычисляет статистику по операциям и записывает ее в указанные файлы
     *
     * @param operationsFileName    имя файла, который содержит список операций
     * @param sumsByDatesFileName   имя файла, в который будут записываться суммы по операцям сгруппированных по датам
     * @param sumsByOfficesFileName имя файла, в который будет записываться суммы по операциям сгруппированных по
     *                              офисам и отсортированных в порядке уменьшения суммы
     * @throws IOException возникает в случае ошибок чтения/записи файлов
     */
    public void calculateAndWriteToFiles(
            String operationsFileName,
            String sumsByDatesFileName,
            String sumsByOfficesFileName) throws IOException, ParseException {

        try (FileInputStream operationsInputStream = new FileInputStream(operationsFileName);
             FileOutputStream sumsByDateOutputStream = new FileOutputStream(sumsByDatesFileName)) {
            calculateAndWriteToStreams(operationsInputStream, sumsByDateOutputStream,
                    new FileOutputStream(sumsByOfficesFileName));
        }
    }

    /**
     * Вычисляет статистику по операциям и записывает ее в указанные потоки
     *
     * @param operationsInputStream     поток, который содержит список операций
     * @param sumsByDateOutputStream    поток, в который будут записываться суммы по операцям сгруппированных по датам
     * @param sumsByOfficesOutputStream поток, в который будет записываться суммы по операциям сгруппированных по
     *                                  офисам и отсортированных в порядке уменьшения суммы
     * @throws IOException возникает в случае ошибок при чтении/записи потока
     */
    public void calculateAndWriteToStreams(
            InputStream operationsInputStream,
            OutputStream sumsByDateOutputStream,
            OutputStream sumsByOfficesOutputStream) throws IOException, ParseException {

        Map<String, BigDecimal> sumsByDates = new HashMap<>();
        Map<String, BigDecimal> sumsByOffices = new HashMap<>();

        try (InputStreamReader reader = new InputStreamReader(operationsInputStream, StandardCharsets.UTF_8);
             BufferedReader bufferedReader = new BufferedReader(reader)) {

            for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {
                String[] splitedLine = line.split(", ");
                if (splitedLine.length < 5) {
                    throw new IllegalArgumentException("Specified incorrect column number in line: " + line);
                }

                BigDecimal operationSum = parseSum(splitedLine[4]);

                String date = splitedLine[0];
                sumsByDates.merge(date, operationSum, BigDecimal::add);

                String officeNumber = splitedLine[2];
                sumsByOffices.merge(officeNumber, operationSum, BigDecimal::add);
            }
        }

        writeStatistic(sumsByDateOutputStream, sumsByDates, Comparator.comparing(Map.Entry::getKey));
        writeStatistic(sumsByOfficesOutputStream, sumsByOffices,
                (o1, o2) -> o2.getValue().compareTo(o1.getValue()));
    }

    private BigDecimal parseSum(String sumString) throws ParseException {
        DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        format.setParseBigDecimal(true);
        return (BigDecimal) format.parse(sumString);
    }

    private static void writeStatistic(
            OutputStream outputStream,
            Map<String, BigDecimal> groupedSums,
            Comparator<Map.Entry<String, BigDecimal>> comparator) throws IOException {

        List<Map.Entry<String, BigDecimal>> sumsByOfficesSorted = groupedSums.entrySet().stream()
                .sorted(comparator)
                .collect(Collectors.toList());

        try (OutputStreamWriter writer = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            for (Map.Entry<String, BigDecimal> entry : sumsByOfficesSorted) {
                bufferedWriter.write(String.format(Locale.US, "%s, %.2f", entry.getKey(), entry.getValue()));
                bufferedWriter.newLine();
            }
        }
    }
}
