package ru.myshkin;

import java.io.IOException;
import java.text.ParseException;

public class Main {

    public static void main(String[] args) throws IOException, ParseException {
        if (args.length < 3) {
            throw new IllegalArgumentException("Three parameters must be specified: [operations file name] " +
                    "[file name for grouped by date sums] [file name for grouped by offices sums]");
        }

        String operationsFileName = args[0];
        String sumsByDatesFileName = args[1];
        String sumsByOfficesFileName = args[2];

        new OperationsStatistic()
                .calculateAndWriteToFiles(operationsFileName, sumsByDatesFileName, sumsByOfficesFileName);
    }
}
