package ru.myshkin;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;

class OperationsStatisticTest {

    private OperationsStatistic operationsStatistic;
    private ByteArrayOutputStream sumsByDatesStream;
    private ByteArrayOutputStream sumsByOfficesStream;

    @BeforeEach
    void setUp() {
        operationsStatistic = new OperationsStatistic();
        sumsByDatesStream = new ByteArrayOutputStream();
        sumsByOfficesStream = new ByteArrayOutputStream();
    }

    @Test
    public void calculate_noOperations_generateEmptyStatistic() throws IOException, ParseException {
        //given
        InputStream operationsStream = new ByteArrayInputStream("".getBytes());

        //when
        operationsStatistic.calculateAndWriteToStreams(operationsStream, sumsByDatesStream, sumsByOfficesStream);

        //then
        Assertions.assertEquals(0, sumsByDatesStream.toByteArray().length);
        Assertions.assertEquals(0, sumsByOfficesStream.toByteArray().length);
    }

    @Test
    public void calculate_correctOperations_generateCorrectStatistic() throws IOException, ParseException {
        //given
        String[] operations = {
                "2017-05-10, 19:15, 1, 0, 15000.25",
                "2017-06-09, 14:55, 2, 1, 70000.75",
                "2017-06-09, 06:36, 2, 2, 30000.25",
                "2017-05-10, 22:55, 1, 3, 10000.25",
                "2017-05-10, 02:40, 1, 4, 25000.50"};
        ByteArrayInputStream operationsStream =
                new ByteArrayInputStream(String.join("\n", operations).getBytes());

        //when
        operationsStatistic.calculateAndWriteToStreams(operationsStream, sumsByDatesStream, sumsByOfficesStream);

        //then
        checkSumsByDates();
        checkSumsByOffices();
    }

    private void checkSumsByDates() {
        String outputString = new String(sumsByDatesStream.toByteArray(), StandardCharsets.UTF_8);
        String[] lines = outputString.split("\n");

        //First line
        String[] splittedFirstLine = lines[0].split(", ");

        String firstLineDate = splittedFirstLine[0];
        Assertions.assertEquals("2017-05-10", firstLineDate);

        String firstLineSum = splittedFirstLine[1];
        Assertions.assertEquals("50001.00", firstLineSum.trim());

        //Second line
        String[] splittedSecondLine = lines[1].split(", ");

        String secondLineDate = splittedSecondLine[0];
        Assertions.assertEquals("2017-06-09", secondLineDate);

        String secondLineSum = splittedSecondLine[1];
        Assertions.assertEquals("100001.00", secondLineSum.trim());
    }

    private void checkSumsByOffices() {
        String outputString = new String(sumsByOfficesStream.toByteArray(), StandardCharsets.UTF_8);
        String[] lines = outputString.split("\n");

        //First line
        String[] splittedFirstLine = lines[0].split(", ");

        String firstLineOffice = splittedFirstLine[0];
        Assertions.assertEquals("2", firstLineOffice);

        String firstLineSum = splittedFirstLine[1];
        Assertions.assertEquals("100001.00", firstLineSum.trim());

        //Second line
        String[] splittedSecondLine = lines[1].split(", ");

        String secondLineOffice = splittedSecondLine[0];
        Assertions.assertEquals("1", secondLineOffice);

        String secondLineSum = splittedSecondLine[1];
        Assertions.assertEquals("50001.00", secondLineSum.trim());
    }
}